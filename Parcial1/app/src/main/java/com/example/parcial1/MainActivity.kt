package com.example.parcial1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private var user_final : String = "nicolasa.rojasa@ecci.edu.co"
    private var pass_final : String = "123456789"
    private lateinit var user : EditText
    private lateinit var password : EditText
    private lateinit var errorMsg : TextView
    private lateinit var btnContinue : Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        instances()
        btnContinue.setOnClickListener(){
            val txt_user = user.text
            val txt_pass = password.text
            val msg_errror : String = applicationContext.getString(R.string.error_data)
            if (validateEmpty(txt_user.toString(), txt_pass.toString())){
                if(txt_pass.length >= 4){
                    if (txt_user.toString() == user_final && txt_pass.toString() == pass_final){
                        errorMsg.text = ""
                        setContentView(R.layout.welcome);
                    }else{
                        errorMsg.text = Editable.Factory.getInstance().newEditable(msg_errror)
                    }
                }else{
                    errorMsg.text = Editable.Factory.getInstance().newEditable(msg_errror)
                }
            }
        }
    }
    private fun validateEmpty(user_method : String , pass_method: String): Boolean {
        if (user_method.isEmpty()){
            val user_msg_errror : String = applicationContext.getString(R.string.error_user_required)
            errorMsg.text = Editable.Factory.getInstance().newEditable(user_msg_errror)
            return false
        }else if(pass_method.isEmpty()){
            val pass_msg_errror : String = applicationContext.getString(R.string.error_pass_required)
            errorMsg.text = Editable.Factory.getInstance().newEditable(pass_msg_errror)
            return false
        }
        errorMsg.text = ""
        return true
    }
    private fun instances() {
        user = findViewById(R.id.editTextUser)
        password = findViewById(R.id.editTextPassword)
        errorMsg = findViewById(R.id.errorMsg)
        btnContinue = findViewById(R.id.btnContinue)
    }

}