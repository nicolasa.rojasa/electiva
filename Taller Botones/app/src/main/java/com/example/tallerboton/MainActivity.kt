package com.example.tallerboton

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private lateinit var btn1 : Button
    private lateinit var btn2 : Button
    private lateinit var btn3 : Button
    private lateinit var btn4 : Button
    private lateinit var btn5 : Button

    private lateinit var txtEntry : EditText
    private lateinit var txtEnd : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        instancias()

        eventoBoton(btn1, "#89ce28")
        eventoBoton(btn2, "#e60000")
        eventoBoton(btn3, "#c3bfb8")
        eventoBoton(btn4, "#ba62c5")
        eventoBoton(btn5, "#0099c8")

    }

    private fun instancias() {
        btn1 = findViewById(R.id.btn1)
        btn2 = findViewById(R.id.btn2)
        btn3 = findViewById(R.id.btn3)
        btn4 = findViewById(R.id.btn4)
        btn5 = findViewById(R.id.btn5)

        txtEntry = findViewById(R.id.txtEntry)
        txtEnd = findViewById(R.id.txtEnd)
    }

    private fun eventoBoton(btn : Button, color: String) {
        btn.setOnClickListener(){
            val txt = txtEntry.text
            if (txt.length > 0){
                txtEnd.text = txt
                txtEntry.text.clear()
                txtEnd.setBackgroundColor(Color.parseColor(color))
            }
        }
    }
}