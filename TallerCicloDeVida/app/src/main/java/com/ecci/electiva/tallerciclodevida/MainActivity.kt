package com.ecci.electiva.tallerciclodevida

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

private lateinit var text1:TextView
private lateinit var text2:TextView
private lateinit var btnActivity1:Button

var counterAppeared = 0
var counterMissing = 0

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        text1 = findViewById(R.id.aparecido1)
        text2 = findViewById(R.id.desaparecido1)
        btnActivity1 = findViewById(R.id.btnActivity1)
        btnActivity1.setOnClickListener {
            val intent = Intent(this, SecondActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        counterAppeared += 1
        toView()
    }
    override fun onStop() {
        super.onStop()
        counterMissing += 1
        toView()
    }

    private fun toView(){
        if (counterMissing == 1)
            text2.text = "$counterMissing vez"
        else
            text2.text = "$counterMissing veces"

        if (counterAppeared == 1)
            text1.text = "$counterAppeared vez"
        else
            text1.text = "$counterAppeared veces"
    }
}