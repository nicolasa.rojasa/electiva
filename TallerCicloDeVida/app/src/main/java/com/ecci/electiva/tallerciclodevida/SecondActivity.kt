package com.ecci.electiva.tallerciclodevida

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

private lateinit var textA1: TextView
private lateinit var textA2: TextView
private lateinit var btnActivity2: Button

var counterAppeared2 = 0
var counterMissing2 = 0

class SecondActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        textA1 = findViewById(R.id.aparecido2)
        textA2 = findViewById(R.id.desaparecido2)
        btnActivity2 = findViewById(R.id.btnActivity2)
        btnActivity2.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onResume() {
        super.onResume()
        counterAppeared2 += 1
        toView()
    }
    override fun onStop() {
        super.onStop()
        counterMissing2 += 1
        toView()
    }

    private fun toView(){
        if (counterAppeared2 == 1)
            textA1.text = "$counterAppeared2 vez"
        else
            textA1.text = "$counterAppeared2 veces"

        if (counterMissing2 == 1)
            textA2.text = "$counterMissing2 vez"
        else
            textA2.text = "$counterMissing2 veces"
    }
}