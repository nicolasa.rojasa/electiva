package com.ecci.electiva.parcial3.data.model

import com.google.gson.annotations.SerializedName

data class Region(
    @SerializedName("name")
    val name: String,
    @SerializedName("alpha2Code")
    val alpha2Code: String,
    @SerializedName("flag")
    val flag: String)