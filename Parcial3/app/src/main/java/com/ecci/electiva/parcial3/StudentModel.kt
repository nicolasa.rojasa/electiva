package com.ecci.electiva.parcial3

import android.net.Uri

data class StudentModel(var name: String, var location: String, var img: Int, var latitude: Double, var longitude: Double)