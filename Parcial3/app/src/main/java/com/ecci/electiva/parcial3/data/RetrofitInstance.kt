package com.ecci.electiva.parcial3.data

import com.ecci.electiva.parcial3.data.api.RegionApi
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

class RetrofitInstance {
    private fun getInstance(): Retrofit{
        val gson = GsonBuilder().create()
        return Retrofit.Builder()
            .baseUrl("https://restcountries.eu/rest/v2/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    fun regionApi(): RegionApi{
        return getInstance().create(RegionApi::class.java)
    }
}