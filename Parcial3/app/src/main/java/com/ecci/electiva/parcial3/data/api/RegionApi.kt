package com.ecci.electiva.parcial3.data.api

import retrofit2.Call
import com.ecci.electiva.parcial3.data.model.Region
import retrofit2.http.GET

interface RegionApi {
    @GET("region/Americas")
    fun getRegions(): Call<List<Region>>
}