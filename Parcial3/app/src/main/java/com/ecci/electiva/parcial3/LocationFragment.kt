package com.ecci.electiva.parcial3

import android.annotation.SuppressLint
import android.graphics.Camera
import android.location.Location
import android.os.Bundle
import android.view.View
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.OnSuccessListener


open class LocationFragment(private val latitude: Double = 0.0, private val longitude: Double = 0.0) : SupportMapFragment(), OnMapReadyCallback{


    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
        getMapAsync(this)
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap) {
        p0.addMarker(MarkerOptions().position(LatLng(2.264243, -73.794342)).title("Nicolas"))
        p0.addMarker(MarkerOptions().position(LatLng(1.100786, -76.627924)).title("Jonathan"))
        p0.addMarker(MarkerOptions().position(LatLng(6.217140, -75.166676)).title("Jobstin"))
        p0.addMarker(MarkerOptions().position(LatLng(6.236281, -75.580516)).title("Stiven"))
        // En caso de no encontrar la ubicacion toma la siguiente por defecto

        var ubication = LatLng(  4.687977, -74.088595)
        fusedLocationClient.lastLocation.addOnSuccessListener { location ->
            ubication = LatLng(location.latitude, location.longitude)
        }
        if (longitude != 0.0 && latitude != 0.0) {
            ubication = LatLng(latitude, longitude)
            p0.moveCamera(CameraUpdateFactory.newLatLngZoom(ubication, 15f))
        }else{
            p0.moveCamera(CameraUpdateFactory.newLatLngZoom(ubication, 5f))
        }
    }
}
