package com.ecci.electiva.parcial3

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.LinearLayoutManager
import com.ecci.electiva.parcial3.data.RetrofitInstance
import com.ecci.electiva.parcial3.data.model.Region
import com.ecci.electiva.parcial3.databinding.FragmentServicesBinding
import com.ecci.electiva.parcial3.databinding.ItemRegionBinding
import com.xwray.groupie.GroupieAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ServicesFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentServicesBinding>(inflater, R.layout.fragment_services, container, false)

        binding.countriesRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        var adapter = GroupieAdapter()

        RetrofitInstance().regionApi().getRegions().enqueue(object: Callback<List<Region>> {
            override fun onResponse( call: Call<List<Region>>, response: Response<List<Region>>
            ) {
                response.body()?.let {
                    val regionItems : List<RegionItem> = it.map { RegionItem(it, requireContext()) }
                    adapter.update(regionItems)
                }
            }

            override fun onFailure(call: Call<List<Region>>, t: Throwable) {
                Toast.makeText(activity, "El servicio ha fallado", Toast.LENGTH_SHORT).show()
            }
        })


        binding.countriesRecyclerView.adapter = adapter

        return binding.root
    }

}