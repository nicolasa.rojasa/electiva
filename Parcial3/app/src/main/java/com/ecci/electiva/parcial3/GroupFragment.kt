package com.ecci.electiva.parcial3

import android.app.Activity.RESULT_OK
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.provider.MediaStore
import android.content.Intent
import android.graphics.Bitmap
import android.widget.ImageView
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_group.*

class GroupFragment : Fragment() {

    val REQUEST_CODE_GALLERY = 1
    val REQUEST_CODE_CAMERA = 2

    private lateinit var adapter: StudentAdapter
    private lateinit var imgUpdate: ImageView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_group, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupList()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun setupList() {
        val students = mutableListOf(
                StudentModel("Nicolas Rojas", "Caño Cristales", R.drawable.nicolas, 2.264243, -73.794342),
                StudentModel("Stiven Paez", "Pueblito paisa", R.drawable.stiven, 6.236281, -75.580516),
                StudentModel("Jobstin Valencia", "Piedra el peñol", R.drawable.jobstin, 6.217140, -75.166676),
                StudentModel("Jonathan Arciniegas", "Cascada fin del mundo", R.drawable.jonathan, 1.100786, -76.627924))

        adapter = StudentAdapter(students){item, imageView, isNumber ->
            if (isNumber == 1){
                selectFromCamera()
                imgUpdate = imageView
            } else if (isNumber == 2){
                pickerFromGallery()
                imgUpdate = imageView
            } else {
                makeCurrentFragment(LocationFragment(item.latitude, item.longitude))
            }
        }
        listStudentRecyclerView.adapter = adapter
        listStudentRecyclerView.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun pickerFromGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, REQUEST_CODE_GALLERY)
    }

    private fun selectFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, REQUEST_CODE_CAMERA)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        when(requestCode) {
            REQUEST_CODE_GALLERY -> {
                if(resultCode == RESULT_OK) {
                    val uri = intent?.data
                    imgUpdate.setImageURI(uri)
                }
            }
            REQUEST_CODE_CAMERA -> {
                if(resultCode == RESULT_OK) {
                    val bitmap = intent?.extras?.get("data") as Bitmap
                    imgUpdate.setImageBitmap(bitmap)
                }
            }
        }
    }
    private fun makeCurrentFragment(fragment: Fragment){
        val transaction = parentFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

}