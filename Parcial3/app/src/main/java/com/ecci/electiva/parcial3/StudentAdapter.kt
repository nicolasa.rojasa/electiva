package com.ecci.electiva.parcial3

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_student.view.*
import android.widget.ImageView

class StudentAdapter(val students: MutableList<StudentModel>,  val callback: (StudentModel, ImageView, Int) -> Unit): RecyclerView.Adapter<StudentAdapter. StudentViewHolder>() {

    class StudentViewHolder(itemView: View, val callback: (StudentModel, ImageView, Int) -> Unit) :
            RecyclerView.ViewHolder(itemView) {

        fun bind(item: StudentModel) {
            itemView.image.setImageResource(item.img)
            itemView.text.text = item.name
            itemView.textLocation.text = item.location
            itemView.photo.setOnClickListener {
                callback(item, itemView.image, 1)
            }
            itemView.gallery.setOnClickListener {
                callback(item, itemView.image, 2)
            }
            itemView.map.setOnClickListener {
                callback(item, itemView.image, 3)
            }
        }


    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_student, parent, false)
        return StudentViewHolder(view, callback)
    }

    override fun onBindViewHolder(holder: StudentViewHolder, position: Int) {
        holder.bind(students[position])
    }

    override fun getItemCount(): Int {
        return students.size
    }
}
