package com.ecci.electiva.parcial3.data.model

data class Country(val name: String, val alpha2Code: String, val flag: String, val capital: String, val population: String)
