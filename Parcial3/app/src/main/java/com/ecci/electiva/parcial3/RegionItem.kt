package com.ecci.electiva.parcial3

import android.content.Context
import android.net.Uri
//import com.bumptech.glide.Glide
import com.ecci.electiva.parcial3.data.model.Region
import com.ecci.electiva.parcial3.databinding.ItemRegionBinding
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.xwray.groupie.databinding.BindableItem


class RegionItem (private val region: Region, val context: Context): BindableItem<ItemRegionBinding>(){
    override fun bind(viewBinding: ItemRegionBinding, position: Int) {
        //Picasso.get().load(region.flag).into(viewBinding.imgCountry)
        GlideToVectorYou.init().with(context).load(Uri.parse(region.flag),viewBinding.imgCountry);

        viewBinding.nameCountry.text = region.name
        viewBinding.descriptionCountry.text = "Code: " + region.alpha2Code
    }


    override fun getLayout(): Int {
        return R.layout.item_region
    }
}