package com.ecci.electiva.parcial2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main2.*

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)


        makeCurrentFragment(Fragment1())


        setupButtons()
    }

    private fun setupButtons() {
        backActivity.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        firstFragment.setOnClickListener {
            makeCurrentFragment(Fragment1())
        }

        secondFragment.setOnClickListener {
            makeCurrentFragment(Fragment2())
        }
    }

    private fun makeCurrentFragment(fragment: Fragment){
        val manager = supportFragmentManager.beginTransaction()
        manager.replace(R.id.contentFragments, fragment)
        manager.addToBackStack(null)
        manager.commit()
    }
}