package com.ecci.electiva.parcial2

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import kotlinx.android.synthetic.main.dialog_student.*


class StudentDialog(context: Context, val name: String,val lastName: String, val identification: String, val phone: String, private val callback: (String, String, String, String) -> Unit) : Dialog(context) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_student)
        nameEditText.setText(name)
        lastNameEditText.setText(lastName)
        phoneEditText.setText(phone)
        identificacionEditText.setText(identification)
        saveButton.setOnClickListener {
            val name = nameEditText.text.toString()
            val lastName = lastNameEditText.text.toString()
            val phone = phoneEditText.text.toString()
            val identificacion = identificacionEditText.text.toString()
            callback(name, lastName, phone, identificacion)
            dismiss()
        }

    }
}