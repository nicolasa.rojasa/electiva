package com.ecci.electiva.parcial2

data class StudentModel(var name: String, var lastName: String, var identification: String, var phone: String)