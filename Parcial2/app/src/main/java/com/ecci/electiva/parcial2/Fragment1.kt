package com.ecci.electiva.parcial2

import android.app.DatePickerDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import kotlinx.android.synthetic.main.fragment_1.*
import java.util.*

class Fragment1 : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_1, container, false)
        val newDate: Button = view.findViewById(R.id.new_date)
        val cal = Calendar.getInstance()
        newDate.setOnClickListener {
            var day = cal.get(Calendar.DAY_OF_MONTH)
            var month = cal.get(Calendar.MONTH)
            var year = cal.get(Calendar.YEAR)
            val datePick =
                context?.let { it1 ->
                    DatePickerDialog(
                        it1,
                        DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                            date.setText("" + dayOfMonth + "/" + month + "/" + year)
                        },
                        year,
                        month,
                        day
                    )
                }
            if (datePick != null) {
                datePick.show()
            }
        }
        return view
    }
}