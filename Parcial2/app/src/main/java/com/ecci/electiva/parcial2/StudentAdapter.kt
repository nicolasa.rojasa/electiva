package com.ecci.electiva.parcial2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_student.view.*

class StudentAdapter(val students: MutableList<StudentModel>, val callback: (StudentModel, Boolean) -> Unit): RecyclerView.Adapter<StudentAdapter. StudentViewHolder>() {
    class StudentViewHolder(itemView: View, val callback: (StudentModel, Boolean) -> Unit) :
        RecyclerView.ViewHolder(itemView) {

        fun bind(item: StudentModel) {
            itemView.nameTextView.text = item.name + " " + item.lastName
            itemView.identificacionTextView.text = item.identification
            itemView.phoneTextView.text = item.phone
            itemView.deleteButton.setOnClickListener {
                callback(item, true)
            }
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_student, parent, false)
        return StudentViewHolder(view, callback)
    }

    override fun onBindViewHolder(holder: StudentViewHolder, position: Int) {
          holder.bind(students[position])
    }

    override fun getItemCount(): Int {
        return students.size
    }

    fun addStudent(product: StudentModel) {
        students.add(product)
    }

    fun deleteStudent(product: StudentModel) {
        students.remove(product)
    }
}