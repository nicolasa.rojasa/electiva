package com.ecci.electiva.parcial2

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var adapter: StudentAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupButtons()
        setupList()
    }

    private fun setupButtons() {
        addButton.setOnClickListener {
            val dialog = StudentDialog(this, "", "", "", "") { name, lastName, identification, phone ->
                addStudent(name, lastName, identification, phone)
            }
            dialog.show()
        }
        secondActivity.setOnClickListener {
            val intent = Intent(this, MainActivity2::class.java)
            startActivity(intent)
        }
    }

    private fun setupList() {
        val students = mutableListOf(
            StudentModel("Nicolas", "Rojas", "123", "123"),
            StudentModel("Stiven", "Paez", "456", "456"),
            StudentModel("Jobstin", "Valencia", "789", "789"),
            StudentModel("Jonathan", "Arciniegas", "061", "061"))

        adapter = StudentAdapter(students) { item, isDelete ->
            if(isDelete) deleteStudent(item)
        }
        studentRecyclerView.adapter = adapter
        studentRecyclerView.layoutManager = LinearLayoutManager(this)
    }

    private fun addStudent(name: String, lastName: String, identification: String, phone: String){
        val student = StudentModel(name, lastName,identification, phone)
        adapter.addStudent(student)
        adapter.notifyDataSetChanged()
    }

    private fun deleteStudent(student: StudentModel) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("ALERTA")
        builder.setMessage("Esta seguro de eliminar este registro ?")
        builder.setPositiveButton("SI"){_, _ ->
            adapter.deleteStudent(student)
            adapter.notifyDataSetChanged()
        }
        builder.setNegativeButton("NO", null)
        builder.show()
    }
}