package com.ecci.electiva.parcial2

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import kotlinx.android.synthetic.main.fragment_2.*

class Fragment2 : Fragment() {

    private val img1 = R.drawable.img_1
    private val img2 = R.drawable.img_2
    private val img3 = R.drawable.img_3
    private val img4 = R.drawable.img_4
    private val img5 = R.drawable.img_5
    private val images = listOf(img1, img2, img3, img4, img5)
    var position:Int = 0


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_2, container, false)

        val nextImg: Button = view.findViewById(R.id.next_img)
        val backImg: Button = view.findViewById(R.id.back_img)

        nextImg.setOnClickListener {
            image.setImageResource(viewImg(true))
        }
        backImg.setOnClickListener {
            image.setImageResource(viewImg(false))
        }
        return view
    }

    private fun viewImg(isSum: Boolean): Int {
        if (isSum){
            position += 1
        }else{
            position -= 1
        }

        if (position < 0){
            position = images.size - 1
        }else if(position >= images.size){
            position = 0
        }
        return images[position]
    }
}