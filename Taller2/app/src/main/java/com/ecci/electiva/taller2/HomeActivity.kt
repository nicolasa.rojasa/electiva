package com.ecci.electiva.taller2

import android.content.Context
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import android.widget.ScrollView
import android.widget.TextView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class HomeActivity : AppCompatActivity(), Communicator {

    private lateinit var btnView: BottomNavigationView

    private lateinit var firstName: String
    private lateinit var lastName: String
    private lateinit var documentType: String
    private lateinit var document: String
    private lateinit var date: String
    private lateinit var hobbies: ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        instances()

        firstName = intent.getStringExtra("firstName").toString()
        lastName = intent.getStringExtra("lastName").toString()
        documentType = intent.getStringExtra("documentType").toString()
        document = intent.getStringExtra("document").toString()
        date = intent.getStringExtra("date").toString()
        hobbies = intent.getStringArrayListExtra("hobbies") as ArrayList<String>


        val infoUserFragment =
            hobbies?.let { InfoUserFragment(firstName, lastName, documentType, document, date, it) }
        val modifyHobbiesFragment = ModifyHobbiesFragment()
        if (infoUserFragment != null) {
            makeCurrentFragment(infoUserFragment, hobbies)
        }


        btnView.setOnNavigationItemReselectedListener {
            when(it.itemId){
                R.id.ic_home -> infoUserFragment?.let { it1 -> makeCurrentFragment(it1, hobbies) }
                R.id.ic_hobbies -> makeCurrentFragment(modifyHobbiesFragment, hobbies)
            }
        }
    }

    private fun makeCurrentFragment( fragment: Fragment, listHobbies: ArrayList<String>){
        val bundle = Bundle()
        bundle.putStringArrayList("listHobbies", listHobbies)
        val manager = supportFragmentManager.beginTransaction()
        manager.apply {
            fragment.arguments = bundle
            replace(R.id.container_content, fragment)
            commit()
        }
    }

    private fun instances(){
        btnView = findViewById(R.id.bottom_navigation)
    }

    override fun passDataCom(listHobbies: ArrayList<String>) {
        hobbies.remove("Yoga")
        hobbies.remove("Futbol")
        hobbies.remove("Cine")
        hobbies.remove("Leer")
        hobbies.remove("Ciclismo")
        hobbies.remove("Videojuegos")
        hobbies = listHobbies
        val bundle = Bundle()
        bundle.putStringArrayList("listHobbies", listHobbies)
        val transaction = this.supportFragmentManager.beginTransaction()
        val infoUserFragment = InfoUserFragment(firstName, lastName, documentType, document, date, listHobbies)
        infoUserFragment.arguments = bundle
        transaction.replace(R.id.container_content, infoUserFragment)
        transaction.hide(ModifyHobbiesFragment())
        transaction.commit()
    }
}