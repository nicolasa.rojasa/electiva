package com.ecci.electiva.taller2

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

class ModifyHobbiesFragment : Fragment() {

    var hobbiesWithFragment : java.util.ArrayList<String>? = arrayListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_modify_hobbies, container, false)
        val modifyHobbies: Button = view.findViewById(R.id.modifyHobbies)
        modifyHobbies.setOnClickListener {
            val communicator: Communicator = activity as Communicator
            communicator.passDataCom(viewHobbies())
            alertBasic("Ha modificado los hobbies")
        }
        return view
    }


    private fun viewHobbies(): ArrayList<String>{
        val listHobbies:ArrayList<String> = arrayListOf("")
        listHobbies.remove("")
        val yoga = view?.findViewById<CheckBox>(R.id.yoga)
        val futbol = view?.findViewById<CheckBox>(R.id.futbol)
        val cine = view?.findViewById<CheckBox>(R.id.cine)
        val leer = view?.findViewById<CheckBox>(R.id.leer)
        val ciclismo = view?.findViewById<CheckBox>(R.id.ciclismo)
        val videojuegos = view?.findViewById<CheckBox>(R.id.videojuegos)
        if (yoga != null && yoga.isChecked)
            listHobbies.add(yoga.text.toString())
        if (futbol != null && futbol.isChecked)
            listHobbies.add(futbol.text.toString())
        if (cine != null && cine.isChecked)
            listHobbies.add(cine.text.toString())
        if (leer != null && leer.isChecked)
            listHobbies.add(leer.text.toString())
        if (ciclismo != null && ciclismo.isChecked)
            listHobbies.add(ciclismo.text.toString())
        if (videojuegos != null && videojuegos.isChecked)
            listHobbies.add(videojuegos.text.toString())
        return listHobbies
    }

    private fun alertBasic( message: String){
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("AVISO")
        builder.setMessage(message)
        builder.setPositiveButton("OK"){_, _ ->
        }
        builder.show()
    }

}