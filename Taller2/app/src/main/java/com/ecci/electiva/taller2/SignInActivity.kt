package com.ecci.electiva.taller2

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.widget.*
import java.util.*
import kotlin.collections.ArrayList


class SignInActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener {

    private lateinit var container: ScrollView
    private lateinit var firstName: EditText
    private lateinit var lastName: EditText
    private lateinit var documentType: TextView
    private lateinit var document: EditText
    private lateinit var dateForm: TextView
    private lateinit var hobbies: TextView
    private lateinit var password: EditText
    private lateinit var repeatPassword: EditText
    private lateinit var btnSubmit: Button
    private lateinit var btnReset: Button

    var day = 0
    var month = 0
    var year = 0
    var savedDay = 0
    var savedMonth = 0
    var savedYear = 0

    var listHobbies: ArrayList<String> = arrayListOf("")

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        instances()
        hobbies.movementMethod = ScrollingMovementMethod()
        documentType.setOnClickListener{ alertDocumentType() }
        hobbies.setOnClickListener{ alertHobbies() }
        btnReset.setOnClickListener { confirmReset() }
        dateForm.setOnClickListener{
            getDateCalendar()
            DatePickerDialog(this, this, year, month, day).show()
        }
        btnSubmit.setOnClickListener{
            instances()
            if(validateEmptyFields()){
                if (validatePassword()){
                    customDialogTermsAndConditions()
                }else{
                    alertBasic("Las contraseñas no coinciden")
                }
            }else{
                alertBasic("Todos los campos son requeridos")
            }
        }
    }

    private fun getDateCalendar(){
        val cal = Calendar.getInstance()
        day = cal.get(Calendar.DAY_OF_MONTH)
        month = cal.get(Calendar.MONTH)
        year = cal.get(Calendar.YEAR)
    }

    override fun onDateSet(view: DatePicker?, yearPicker: Int, monthPicker: Int, dayOfMonthPicker: Int) {
        savedDay = dayOfMonthPicker
        savedMonth = monthPicker
        savedYear = yearPicker
        getDateCalendar()
        val dayString = intToString(savedDay.toString())
        val monthString = intToString(savedMonth.toString())
        dateForm.setTextColor(-0x1000000)
        dateForm.text = "$dayString/$monthString/$savedYear"
    }

    private fun intToString(text: String): String{
        return if (text.length == 1){
            "0$text"
        }else{
            text
        }
    }

    private fun alertDocumentType(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Tipo de documento")
        builder.setItems(getDocumentsTypes()){_, position ->
            when(position){
                0 -> documentType.text = getDocumentsTypes()[0]
                1 -> documentType.text = getDocumentsTypes()[1]
                2 -> documentType.text = getDocumentsTypes()[2]
                3 -> documentType.text = getDocumentsTypes()[3]
                4 -> documentType.text = getDocumentsTypes()[4]
            }
        }
        documentType.setTextColor(-0x1000000)
        builder.show()
    }

    private fun alertHobbies(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Que hobbies tienes")
        val checkedItems = booleanArrayOf(false, false, false, false, false, false)
        var textHobbies:String = ""
        builder.setMultiChoiceItems(getHobbies(), checkedItems){_, position, isChecked ->
          if (isChecked){
              listHobbies.add(getHobbies()[position])
              textHobbies += "- " + getHobbies()[position] + "\n"
          }
        }
        builder.setPositiveButton("Continuar"){_, _ ->
            hobbies.text = textHobbies
            hobbies.setTextColor(-0x1000000)
        }
        val dialog = builder.create()
        dialog.show()
    }

    private fun confirmReset(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("ALERTA")
        builder.setMessage("Esta seguro de limpiar los campos ?")
        builder.setPositiveButton("Confirmar"){_, _ ->
            resetFields()
        }
        builder.setNegativeButton("Cancelar", null)
        builder.show()
    }

    private fun alertBasic( message: String){
        val builder = AlertDialog.Builder(this)
        builder.setTitle("AVISO")
        builder.setMessage(message)
        builder.setPositiveButton("OK"){_, _ ->
            password.setText("")
            repeatPassword.setText("")
        }
        builder.show()
    }

    private fun getDocumentsTypes():Array<String>{
        return arrayOf(
        "Cedula de ciudadania",
        "Cedula de extranjeria",
        "Num. de identificacion personal",
        "Num. de identificacion tributaria",
        "Tarjeta de identidad")
    }

    private fun getHobbies():Array<String>{
        return arrayOf(
                "Yoga",
                "Futbol",
                "Cine",
                "Leer",
                "Ciclismo",
                "Videojuegos"
        )
    }

    private fun instances(){
        firstName = findViewById(R.id.firstName)
        lastName = findViewById(R.id.lastName)
        documentType = findViewById(R.id.documentType)
        document = findViewById(R.id.document)
        dateForm = findViewById(R.id.dateForm)
        hobbies = findViewById(R.id.hobbies)
        password = findViewById(R.id.password)
        repeatPassword = findViewById(R.id.repeatPassword)
        container = findViewById(R.id.container)
        btnSubmit = findViewById(R.id.btnSubmit)
        btnReset = findViewById(R.id.btnReset)
    }

    private fun validateEmptyFields(): Boolean{
        val validateFirstName = firstName.text.toString()
        val validateLastName = lastName.text.toString()
        val validateDocumentType = documentType.text.toString()
        val validateDocument = document.text.toString()
        val validateDate = dateForm.text.toString()
        val validateHobbies = hobbies.text.toString()
        val validatePassword = password.text.toString()
        val validateRepeatPassword = repeatPassword.text.toString()
        return (validateFirstName != "" && validateLastName != "" && validateDocumentType != "Tipo de documento"
                && validateDocumentType != "" && validateDocument != "" && validateDate != "Fecha de nacimiento" && validateDate != ""
                && validateHobbies != "Hobbies..." && validateHobbies != "" && validatePassword != "" && validateRepeatPassword != "")
    }

    @SuppressLint("ResourceAsColor")
    private fun resetFields(){
        firstName.setText("")
        lastName.setText("")
        documentType.text = "Tipo de documento"
        documentType.setTextColor(R.color.color_placeholder)
        document.setText("")
        dateForm.text = "Fecha de nacimiento"
        dateForm.setTextColor(R.color.color_placeholder)
        hobbies.text = "Hobbies..."
        hobbies.setTextColor(R.color.color_placeholder)
        password.setText("")
        repeatPassword.setText("")
    }

    private fun validatePassword(): Boolean {
        return password.text.toString() == repeatPassword.text.toString()
    }

    private fun customDialogTermsAndConditions(){
        val dialogView = LayoutInflater.from(this).inflate(R.layout.terms_and_conditions_dialog, null)
        val builder = AlertDialog.Builder(this)
        builder.setView(dialogView)
        val alertDialog = builder.show()
        alertDialog.findViewById<Button>(R.id.acceptTerms).setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java).apply {
                putExtra("firstName", firstName.text.toString())
                putExtra("lastName", lastName.text.toString())
                putExtra("documentType", documentType.text)
                putExtra("document", document.text.toString())
                putExtra("date", dateForm.text)
                listHobbies.remove("")
                putStringArrayListExtra("hobbies", listHobbies)
            }
            startActivity(intent)
            alertDialog.dismiss()
        }
        alertDialog.findViewById<Button>(R.id.cancelTerms).setOnClickListener {
            resetFields()
            alertDialog.dismiss()
        }
    }
}