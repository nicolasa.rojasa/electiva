package com.ecci.electiva.taller2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import com.google.android.material.bottomnavigation.BottomNavigationView
import java.util.ArrayList

class InfoUserFragment(private val firstNameAct: String?,
                       private val lastNameAct: String?,
                       private val documentTypeAct: String?,
                       private val documentAct: String?,
                       private val dateAct: String?,
                       private val hobbieAct: ArrayList<String>) : Fragment() {

    private lateinit var nameHome: TextView
    private lateinit var lastNameHome: TextView
    private lateinit var documentTypeHome: TextView
    private lateinit var documentHome: TextView
    private lateinit var dateHome: TextView
    private lateinit var hobbiesHome: ListView
    var hobbiesWithFragment : ArrayList<String>? = arrayListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_info_user, container, false)
        instances(view)
        nameHome.text = firstNameAct
        lastNameHome.text = lastNameAct
        documentTypeHome.text = documentTypeAct
        documentHome.text = documentAct
        dateHome.text = dateAct
        hobbiesWithFragment = arguments?.getStringArrayList("listHobbies")
        val hobbiesList : ArrayList<String> = arrayListOf("")
        hobbiesList.remove("")
        hobbiesWithFragment?.let { hobbiesList.addAll(it) }
        hobbiesHome.adapter =
                activity?.let { ArrayAdapter(it, android.R.layout.simple_list_item_1, hobbiesList) }
        return view
    }

    private fun instances(view: View){
        nameHome = view.findViewById(R.id.nameHome)
        lastNameHome = view.findViewById(R.id.lastNameHome)
        documentTypeHome = view.findViewById(R.id.documentTypeHome)
        documentHome = view.findViewById(R.id.documentHome)
        dateHome = view.findViewById(R.id.dateHome)
        hobbiesHome = view.findViewById(R.id.hobbiesHome)
    }

}