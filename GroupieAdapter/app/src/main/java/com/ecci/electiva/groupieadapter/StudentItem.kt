package com.ecci.electiva.groupieadapter

import com.ecci.electiva.groupieadapter.databinding.ItemStudentBinding
import com.xwray.groupie.databinding.BindableItem

class StudentItem(private val nameStudent: String): BindableItem<ItemStudentBinding>(){
    override fun bind(viewBinding: ItemStudentBinding, position: Int) {
        viewBinding.nameTextView.text = nameStudent
    }

    override fun getLayout(): Int {
        return R.layout.item_student
    }
}