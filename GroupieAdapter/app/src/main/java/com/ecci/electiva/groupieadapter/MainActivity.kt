package com.ecci.electiva.groupieadapter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.ecci.electiva.groupieadapter.databinding.ActivityMainBinding
import com.xwray.groupie.GroupieAdapter

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
        binding.countriesRecyclerView.layoutManager = LinearLayoutManager(this)


        var adapter = GroupieAdapter()
        adapter.add(StudentItem("Nicolas Arbey Rojas Arias / 53127"))
        adapter.add(StudentItem("Stiven Paez Diaz / 51724"))
        adapter.add(StudentItem("Jonathan Arciniegas Torres / 51996"))
        adapter.add(StudentItem("Jobstin Fabian Valencia Diaz / 48714"))

        binding.countriesRecyclerView.adapter = adapter
    }
}